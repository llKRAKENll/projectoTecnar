import sys, os
import hashlib
import re
import time
import socket

from ui_login import *
from sistema import venSistema
from alchemy import dbaogin, dba
from sqlalchemy.exc import *
from datetime import datetime, timedelta

class venLogin(QtWidgets.QDialog):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.centrarVentana()
        self.venS = venSistema()
        try:
            dbaogin.query.all()
            self.enableLogin()
        except ProgrammingError as e:
            print("Error al conectar con la database"+'\n'+str(e.args))
            self.disableLogin()
        except InvalidRequestError as e:
            print("Error al conectar con la database"+'\n'+str(e.args))
            self.disableLogin()
        except OperationalError as e:
            print("Error al conectar con la database"+'\n'+str(e.args))
            self.disableLogin()
        except IntegrityError as e:
            print("Error al conectar con la database"+'\n'+str(e.args))
            self.disableLogin()
    def enableLogin(self):
        QtWidgets.QMessageBox.information(self, "Enable", "Connection enable", QtWidgets.QMessageBox.Ok)
        self.ui.label_3.setText("ON")
        self.ui.label_3.setStyleSheet("color: green")
        self.ui.lStatus.setGeometry(QtCore.QRect(390, 10, 21, 21))
        self.ui.lStatus.setPixmap(QtGui.QPixmap("icons/On.png"))
        self.ui.lineEdit_username.textChanged.connect(self.valiLineUser)
        self.ui.lineEdit_password.textChanged.connect(self.valiLinePass)
        self.ui.pushButton.clicked.connect(self.connectionLogin)
            
    def disableLogin(self):
        QtWidgets.QMessageBox.warning(self, "Disable", "ERROR!"+"\n"+"Failed to Connect DB.", QtWidgets.QMessageBox.Ok)
        self.ui.label_3.setText("OFF")
        self.ui.label_3.setStyleSheet("color: red")
        self.ui.lStatus.setGeometry(QtCore.QRect(390, 10, 21, 21))
        self.ui.lStatus.setPixmap(QtGui.QPixmap("icons/Off.png"))
        self.ui.lineEdit_username.setDisabled(True)
        self.ui.lineEdit_password.setDisabled(True)
        self.ui.pushButton.setDisabled(True)
                
    def valiLineUser(self):
        users = self.ui.lineEdit_username.text()
        validateU = re.match('^[a-zA-Z0-9\sáéíóúàèìòùäëïöüñ._-]+$', users, re.I)
        if users == "":
            self.ui.lineEdit_username.setStyleSheet("border: 1px solid red;")
            self.ui.label_notice.setText("La casilla de usuario esta vacia.")
            self.ui.label_notice.setStyleSheet("color: red")
            return False
        elif not validateU:
            self.ui.lineEdit_username.setStyleSheet("border: 1px solid red;")
            self.ui.label_notice.setText("Solo numeros y letras.")
            self.ui.label_notice.setStyleSheet("color: red")
            return False
        else:
            self.ui.lineEdit_username.setStyleSheet("border: 1px solid cyan;")
            self.ui.lineEdit_username.setStyleSheet("border: 1px solid cyan;")
            self.ui.label_notice.setText("")
            return True

    def valiLinePass(self):
        passwd = self.ui.lineEdit_password.text()
        validateP = re.match('^[a-zA-Z0-9\sáéíóúàèìòùäëïöüñ.!_"--@#$%&/()=?¡¿+*,;:|''~°]+$', passwd, re.I)
        if passwd == "":
            self.ui.lineEdit_password.setStyleSheet("border: 1px solid red;")
            self.ui.label_notice.setText("La casilla de password esta vacia.")
            self.ui.label_notice.setStyleSheet("color: red")
            return False
        elif not validateP:
            self.ui.lineEdit_password.setStyleSheet("border: 1px solid red;")
            return False
        else:
            self.ui.lineEdit_password.setStyleSheet("border: 1px solid cyan;")
            self.ui.label_notice.setText("")
            return True

    def connectionLogin(self):
        if self.valiLineUser() and self.valiLinePass():
            users = self.ui.lineEdit_username.text()
            key = str(self.ui.lineEdit_password.text())
            passwd = hashlib.sha256(key.encode()).hexdigest()

            if self.ui.radioButtonDev.isChecked() == True:
                dev = '0'
                query = dbaogin.query.filter_by(usuario=users,clave=passwd,tipo=dev).first()
                query2 = dbaogin.query.filter_by(usuario=users).first()
                if query is not None and query2 is not None:
                    if query2.status == 0:
                        QtWidgets.QMessageBox.information(self, "Enable", "Welcomen "+users+"")
                        if query2.intentos == 0:
                            self.venS.show()
                            self.close()
                        else:
                            result = dbaogin.query.filter_by(id=query2.id).first()
                            result.intentos = 0
                            dba.session.commit()
                            self.venS.show()
                            self.close()
                        
                    elif datetime.now() > query2.timestatus:
                        result = dbaogin.query.filter_by(id=query2.id).first()
                        result.status = 0
                        dba.session.commit()
                        QtWidgets.QMessageBox.information(self, "Enable", "Welcomen "+users+"")
                        self.venS.show()
                        self.close()
                    else:
                        QtWidgets.QMessageBox.warning(self, "Bloqueo", "Cuenta bloqueada por exceso de intentos"+"\n"+"1 Hora el bloqueo")
                else:
                    if query2 is None:
                        QtWidgets.QMessageBox.warning(self, "Disable", "Cuenta no existe.")
                    else:
                        if query2.status == 0:
                            if query2.intentos == 5:
                                result = dbaogin.query.filter_by(id=query2.id).first()
                                result.intentos = 0
                                result.status = 1
                                result.timestatus = datetime.now()+timedelta(hours=1)
                                dba.session.commit()
                                QtWidgets.QMessageBox.warning(self, "Bloqueo", "Usted a hecho mas de 5 intentos, se procede a bloquiar la cuenta por 1 hora.")
                            else:
                                bloqueo = query2.intentos
                                bloqueo += 1
                                result = dbaogin.query.filter_by(id=query2.id).first()
                                result.intentos = bloqueo
                                dba.session.commit()
                                QtWidgets.QMessageBox.warning(self, "Disable", "Usuario y Password no es valido. "+"\n"+"Intento: "+str(bloqueo))

            elif self.ui.radioButtonAdmin.isChecked() == True:
                admin = '1'
                query = dbaogin.query.filter_by(usuario=users,clave=passwd,tipo=admin).first()
                if query is not None:
                    QtWidgets.QMessageBox.information(self, "Enable", "Welcomen "+users+"")
                    self.venS.show()
                    self.close()
                else:
                    QtWidgets.QMessageBox.warning(self, "Disable", "Usuario y Password no es valido.")

            elif self.ui.radioButtonStuden.isChecked() == True:
                studen = '2'
                query = dbaogin.query.filter_by(usuario=users,clave=passwd,tipo=studen).first()
                if query is not None:
                    QtWidgets.QMessageBox.information(self, "Enable", "Welcomen "+users+"")
                    self.venS.show()
                    self.close()
                else:
                    QtWidgets.QMessageBox.warning(self, "Disable", "Usuario y Password no es valido.")
            else:
                QtWidgets.QMessageBox.warning(self, "Error", "Por favor selecione su status.")

            
                
    def centrarVentana(self):
        screen = QtWidgets.QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width() - size.width()) / 2, (screen.height() - size.height()) / 2)
        

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    myapp = venLogin()
    myapp.show()
    sys.exit(app.exec_())