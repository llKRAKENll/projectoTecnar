import sys, os
import hashlib
import re
import cv2
import numpy as np
import random

from ui_login import *
from ui_sistema import *
from alchemy import dbNotas, dbregisterStun, db, dba
from sqlalchemy.exc import *


class venSistema(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.centrarVentana()
        
        self.ui.actionGoogle.triggered.connect(self.google)
        self.ui.actionUniversidad.triggered.connect(self.tecnar)
        self.ui.actionSalir.triggered.connect(self.exit)
        self.ui.actionSearch.triggered.connect(self.vsearch)
        self.ui.actionRegistro.triggered.connect(self.vregister)
        self.ui.actionInformacion.triggered.connect(self.vinforamtion)
        self.ui.actionHorario.triggered.connect(self.vcalendar)
        self.ui.actionPuntaje.triggered.connect(self.vpuntaje)

        self.ui.lineEdit_B1codecc.textChanged.connect(self.valiCodeCC)
        self.ui.lineEdit_B3codecc.textChanged.connect(self.valiCodeCC3)


        self.ui.pushButton_R1photo.clicked.connect(self.sacarFoto)
        self.ui.pushButton_guardar.clicked.connect(self.registerStunden)

        

    ################################################################################################################################    
    #MENU                                                                                                                          /
    ################################################################################################################################   
    def exit(self):
        sys.exit()
    def google(self):
        QtGui.QDesktopServices.openUrl(QtCore.QUrl("https://google.com.co"))
    def tecnar(self):
        QtGui.QDesktopServices.openUrl(QtCore.QUrl("http://www.tecnar.edu.co"))
    ################################################################################################################################
    #
    #
    #
    #
    ################################################################################################################################
    #SEARCH                                                                                                                        /
    ################################################################################################################################
    def vsearch(self):
        self.ui.stackedWidget.setCurrentIndex(0)

    def valiCodeCC(self):
        code = self.ui.lineEdit_B1codecc.text()
        validateC = re.match('^[A-Z0-9]+$', code, re.I)

        if code == "":
            self.ui.lineEdit_B1codecc.setStyleSheet("border: 1px solid purple;")
            self.ui.label_B1notice.setText('Cuadro vacio')
            self.ui.label_B1notice.setStyleSheet('color: red')
            return False
        elif not validateC:
            self.ui.lineEdit_B1codecc.setStyleSheet("border: 1px solid red;")
            self.ui.label_B1notice.setText('Solo numeros')
            self.ui.label_B1notice.setStyleSheet('color: red')
            return False
        elif len(code) <= 13:
            self.ui.lineEdit_B1codecc.setStyleSheet("border: 1px solid yellow;")
            self.ui.label_B1notice.setText('')
        else:
            try:
                #Mostrar informacion del estudiante
                self.ui.label_B1notice.setText('')
                self.ui.lineEdit_B1codecc.setStyleSheet("border: 1px solid cyan;")
                query = dbregisterStun.query.filter_by(carnet=code).first()
                
                data = [self.ui.label_B1name.setText(query.apellido+' '+query.nombre),self.ui.lineEdit_B1apellido.setText(query.apellido),
                    self.ui.lineEdit_B1nombre.setText(query.nombre),self.ui.lineEdit_B1email.setText(query.email),
                    self.ui.lineEdit_B1tipo.setText(query.tipo),self.ui.lineEdit_B1documento.setText(str(query.documento)),
                    self.ui.lineEdit_B1pais.setText(query.pais),self.ui.lineEdit_B1departamento.setText(query.departamento),
                    self.ui.lineEdit_B1sexo.setText(query.sexo),self.ui.lineEdit_B1nacimiento.setText(str(query.fecha)),
                    self.ui.lineEdit_B1departamentoN.setText(query.departamento),self.ui.lineEdit_B1ciudadN.setText(query.ciudad),
                    self.ui.lineEdit_B1direccion.setText(query.direccion),self.ui.lineEdit_B1departamentoR.setText(query.departamento),
                    self.ui.lineEdit_B1barrioR.setText(query.barrio),self.ui.lineEdit_B1telef1.setText(query.telefono_1),
                    self.ui.lineEdit_B1telef2.setText(query.telefono_2),self.ui.lineEdit_B1movil.setText(query.movil),
                    self.ui.label_B1photo.setPixmap(QtGui.QPixmap(query.foto)),self.ui.label_B1carnet.setText(query.carnet)]
                for i in range(18):
                    data[i]
            except AttributeError as e:
                QtWidgets.QMessageBox.warning(self, 'Error', 'Fallo.'+'\n'+str(e.args))
            except ValueError as e:
                QtWidgets.QMessageBox.warning(self, 'Error', 'Fallo.'+'\n'+str(e.args))
            except NameError as e:
                QtWidgets.QMessageBox.warning(self, 'Error', 'Fallo.'+'\n'+str(e.args))
            except ProgrammingError as e:
                QtWidgets.QMessageBox.warning(self, 'Error', 'Fallo.'+'\n'+str(e.args))
            except InvalidRequestError as e:
                QtWidgets.QMessageBox.warning(self, 'Error', 'Fallo.'+'\n'+str(e.args))
            except OperationalError as e:
                QtWidgets.QMessageBox.warning(self, 'Error', 'Fallo.'+'\n'+str(e.args))
            except IntegrityError as e:
                QtWidgets.QMessageBox.warning(self, 'Error', 'Fallo.'+'\n'+str(e.args))
            except AttributeError as e:
                QtWidgets.QMessageBox.warning(self, 'Error', 'Fallo.'+'\n'+str(e.args))

    def valiCodeCC3(self):
        code = self.ui.lineEdit_B3codecc.text()
        validateC = re.match('^[A-Z0-9]+$', code, re.I)

        if code == "":
            self.ui.lineEdit_B3codecc.setStyleSheet("border: 1px solid purple;")
            self.ui.label_B3notice.setText('Cuadro vacio')
            self.ui.label_B3notice.setStyleSheet('color: red')
            return False
        elif not validateC:
            self.ui.lineEdit_B3codecc.setStyleSheet("border: 1px solid red;")
            self.ui.label_B3notice.setText('Solo numeros')
            self.ui.label_B3notice.setStyleSheet('color: red')
            return False
        elif len(code) <= 1:
            self.ui.lineEdit_B3codecc.setStyleSheet("border: 1px solid yellow;")
            self.ui.label_B3notice.setText('')
        else:
            try:
                #Mostrar informacion del estudiante
                self.ui.label_B3notice.setText('')
                self.ui.lineEdit_B3codecc.setStyleSheet("border: 1px solid cyan;")
                carnet = self.ui.lineEdit_B3codecc.text()
                p =self.ui.comboBox_B3periodo.currentText()
                s =self.ui.comboBox_B3semestre.currentText()
                query = dbNotas.query.filter_by(carnet=carnet,periodo=p,semestre=s).all()
                info = dbregisterStun.query.filter_by(carnet=carnet).first()

                self.ui.label_B3photo.setPixmap(QtGui.QPixmap(info.foto))
                self.ui.label_B3name.setText(info.apellido+''+info.nombre)
                self.ui.label_B3curso.setText(info.carrera)

                for i, data  in enumerate(query):
                    self.ui.tableWidget_B3.setItem(i, 0, QtWidgets.QTableWidgetItem(data.materia))
                    self.ui.tableWidget_B3.setItem(i, 1, QtWidgets.QTableWidgetItem(str(data.np1)))
                    self.ui.tableWidget_B3.setItem(i, 2, QtWidgets.QTableWidgetItem(str(data.np2)))
                    self.ui.tableWidget_B3.setItem(i, 3, QtWidgets.QTableWidgetItem(str(data.np3)))
                    self.ui.tableWidget_B3.setItem(i, 4, QtWidgets.QTableWidgetItem(str(data.np4)))
                    self.ui.tableWidget_B3.setItem(i, 5, QtWidgets.QTableWidgetItem(str(data.np5)))
                
                
            
            except ProgrammingError as e:
                QtWidgets.QMessageBox.warning(self, 'Error', 'Fallo.'+'\n'+str(e.args))
            

    ################################################################################################################################

    ################################################################################################################################
    #REGISTROS DE ESTUDIANTE                                                                                                       /
    ################################################################################################################################
    def vregister(self):
        self.ui.stackedWidget.setCurrentIndex(1)

    def registerStunden(self):
        try:
            #LineEdit en una lista
            data = [self.ui.lineEdit_R1carnet.text(),self.ui.lineEdit_R1ncc.text(),str(self.ui.comboBox_R1cc.currentText()),
            self.ui.lineEdit_R1apellido.text(),self.ui.lineEdit_R1nombre.text(),str(self.ui.comboBox_R1sexo.currentText()),
            str(self.ui.comboBox_R1carrera.currentText()),self.ui.dateEdit_R1.text(),str(self.ui.comboBox_R1pais.currentText()),
            str(self.ui.comboBox_R1departamento.currentText()),str(self.ui.comboBox_R1ciudad.currentText()),self.ui.lineEdit_R1direccion.text(),
            self.ui.lineEdit_R1barrio.text(),self.ui.lineEdit_R1telef1.text(),self.ui.lineEdit_R1telef2.text(),
            self.ui.lineEdit_R1movil.text(),self.ui.lineEdit_R1email.text(),self.ui.lineEdit_R1apellidoF1.text(),
            self.ui.lineEdit_R1nombreF1.text(),self.ui.lineEdit_R1telefF1.text(),self.ui.lineEdit_R1movilF1.text(),
            self.ui.lineEdit_R1direccionF1.text(),self.ui.lineEdit_R1apellidoA1.text(),self.ui.lineEdit_R1nombreA1.text(),
            self.ui.lineEdit_R1telefA1.text(),self.ui.lineEdit_R1movilA1.text(),self.ui.lineEdit_R1direccionA1.text()]
            foto = 'fotos/'+str(data[1])+'.png'

            #Insertar a la DB Postgres
            query = dbregisterStun(data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7],data[8],data[9],data[10],
                data[11],data[12],data[13],data[14],data[15],data[16],data[17],data[18],data[19],data[20],data[21],data[22],
                data[23],data[24],data[25],data[26],foto)
            dba.session.add(query)
            dba.session.commit()

            QtWidgets.QMessageBox.information(self, 'Genial', 'Inforamcion guardada.')

            #Limpiar LineEdit
            dataclear = [self.ui.lineEdit_R1ncc.clear(),self.ui.lineEdit_R1carnet.clear(),self.ui.lineEdit_R1apellido.clear(),self.ui.lineEdit_R1nombre.clear(),
            self.ui.lineEdit_R1direccion.clear(),self.ui.lineEdit_R1barrio.clear(),self.ui.lineEdit_R1telef1.clear(),
            self.ui.lineEdit_R1telef2.clear(),self.ui.lineEdit_R1movil.clear(),self.ui.lineEdit_R1email.clear(),
            self.ui.lineEdit_R1apellidoF1.clear(),self.ui.lineEdit_R1nombreF1.clear(),self.ui.lineEdit_R1telefF1.clear(),
            self.ui.lineEdit_R1movilF1.clear(),self.ui.lineEdit_R1direccionF1.clear(),self.ui.lineEdit_R1apellidoA1.clear(),
            self.ui.lineEdit_R1nombreA1.clear(),self.ui.lineEdit_R1telefA1.clear(),self.ui.lineEdit_R1movilA1.clear(),
            self.ui.lineEdit_R1direccionA1.clear()]
            self.ui.label_R1photo.setPixmap(QtGui.QPixmap('icons/tecnar.png'))

            for i in range(18):
                dataclear[i]
            
        except AttributeError as e:
            QtWidgets.QMessageBox.warning(self, 'Error', 'Fallo.'+'\n'+str(e.args))
        except ValueError as e:
            QtWidgets.QMessageBox.warning(self, 'Error', 'Fallo.'+'\n'+str(e.args))
        except NameError as e:
            QtWidgets.QMessageBox.warning(self, 'Error', 'Fallo.'+'\n'+str(e.args))
        except ProgrammingError as e:
            QtWidgets.QMessageBox.warning(self, 'Error', 'Fallo.'+'\n'+str(e.args))
        except InvalidRequestError as e:
            QtWidgets.QMessageBox.warning(self, 'Error', 'Fallo.'+'\n'+str(e.args))
        except OperationalError as e:
            QtWidgets.QMessageBox.warning(self, 'Error', 'Fallo.'+'\n'+str(e.args))
        except IntegrityError as e:
            QtWidgets.QMessageBox.warning(self, 'Error', 'Fallo.'+'\n'+str(e.args))
        except AttributeError as e:
            QtWidgets.QMessageBox.warning(self, 'Error', 'Fallo.'+'\n'+str(e.args))
       
        
    def sacarFoto(self):
        cap = cv2.VideoCapture(0)

        if not cap.isOpened():
            QtWidgets.QMessageBox.warning(self, 'Error', 'No hay camara')
        elif self.ui.lineEdit_R1ncc.text() == "":
            QtWidgets.QMessageBox.warning(self, 'Error', 'La casilla de documentacion esta vacia.')
        elif len(self.ui.lineEdit_R1ncc.text()) <= 9:
            QtWidgets.QMessageBox.warning(self, 'Error', 'El numero de documento son de 10 numeros.')
        elif self.ui.lineEdit_R1apellido.text() == "":
            QtWidgets.QMessageBox.warning(self, 'Error', 'La casilla de apellido esta vacia.')
        elif self.ui.lineEdit_R1nombre.text() == "":
            QtWidgets.QMessageBox.warning(self, 'Error', 'La casilla de nombre esta vacia.')
        else:
            while True:
                ret, frame = cap.read()
                cv2.imshow('Camara', frame)
                cv2.imwrite('fotos/'+self.ui.lineEdit_R1ncc.text()+'.png', frame)
                if cv2.waitKey(1) & 0xFF == ord ('f'):
                    self.ui.label_R1photo.setPixmap(QtGui.QPixmap('fotos/'+self.ui.lineEdit_R1ncc.text()+'.png'))
                    break
            cap.release()
            cv2.destroyAllWindows()
            a = self.ui.lineEdit_R1apellido.text()
            n = self.ui.lineEdit_R1nombre.text()
            self.ui.lineEdit_R1carnet.setText(a[0]+n[0]+str(random.randint(1,999999999999)))
    ################################################################################################################################
    ##############
    #INFORMACION
    ###########
    def vinforamtion(self):
        self.ui.stackedWidget.setCurrentIndex(2)
    ################################################################################################################################
    #############
    #CALENDARIO
    ##########
    def vcalendar(self):
        self.ui.stackedWidget.setCurrentIndex(3)
    ################################################################################################################################
    ###########
    #DIAGRAMA
    ########
    def vpuntaje(self):
        self.ui.stackedWidget.setCurrentIndex(4)
    ################################################################################################################################
    
 

    def centrarVentana(self):
        screen = QtWidgets.QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width() - size.width()) / 2, (screen.height() - size.height()) / 5)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    myapp = venSistema()
    myapp.show()
    sys.exit(app.exec_())
