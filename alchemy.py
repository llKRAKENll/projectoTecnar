#import sys
#from PyQt5 import QtSql
#from login import *

#db = QtSql.QSqlDatabase.addDatabase("QPSQL")
#if(db):
#    db.setHostName("localhost")
#    db.setDatabaseName("registroDev")
#    db.setUserName("postgre")
#query = QtSql.QSqlQuery()

from flask import Flask
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:Lb1045723@localhost/Registros' 
app.config ['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
dba = SQLAlchemy(app)

class dbaogin(dba.Model):
    __tablename__ = 'tbl_login'
    id = dba.Column('id', dba.Integer(), primary_key=True) 
    usuario = dba.Column('usuario', dba.String())
    clave = dba.Column('clave', dba.String())
    tipo = dba.Column('tipo', dba.String())
    intentos = dba.Column('intentos', dba.Integer())
    status = dba.Column('status', dba.Integer())
    timestatus = dba.Column('timestatus', dba.String())

    def __init__(self, intentos, status):
    	self.intentos = intentos
    	self.status = status
    	self.timestatus = timestatus

class dbregisterStun(dba.Model):
	__tablename__= 'tbl_register_estudiante'
	id = dba.Column('id', dba.Integer(), primary_key=True)
	carnet = dba.Column('carnet', dba.String(), primary_key=True)
	documento = dba.Column('documento', dba.String())
	tipo = dba.Column('tipo', dba.String())
	apellido = dba.Column('apellido', dba.String())
	nombre = dba.Column('nombre', dba.String())
	sexo = dba.Column('sexo', dba.String())
	carrera = dba.Column('carrera', dba.String())
	fecha = dba.Column('fecha_nacimiento', dba.String())
	pais = dba.Column('pais', dba.String())
	departamento = dba.Column('departamento', dba.String())
	ciudad = dba.Column('ciudad', dba.String())
	direccion = dba.Column('direccion', dba.String())
	barrio = dba.Column('barrio', dba.String())
	telefono_1 = dba.Column('telefono_1', dba.String())
	telefono_2 = dba.Column('telefono_2', dba.String())
	movil = dba.Column('movil', dba.String())
	email = dba.Column('email', dba.String())
	nombre_familiar = dba.Column('nombre_familiar', dba.String())
	apellido_familiar = dba.Column('apellido_familiar', dba.String())
	telefono_familiar = dba.Column('telefono_familiar', dba.String())
	movil_familiar = dba.Column('movil_familiar', dba.String())
	direccion_familiar = dba.Column('direccion_familiar', dba.String())
	nombre_amigo = dba.Column('nombre_amigo', dba.String())
	apellido_amigo = dba.Column('apellido_amigo', dba.String())
	telefono_amigo = dba.Column('telefono_amigo', dba.String())
	movil_amigo = dba.Column('movil_amigo', dba.String())
	direccion_amigo = dba.Column('direccion_amigo', dba.String())
	foto = dba.Column('foto', dba.String())


	def __init__(self, carnet, documento, tipo, apellido, nombre, sexo, carrera, fecha, 
		pais, departamento, ciudad, direccion, barrio, telefono_1, telefono_2, 
		movil, email, apellido_familiar, nombre_familiar, telefono_familiar, 
		movil_familiar, direccion_familiar, apellido_amigo, nombre_amigo, 
		telefono_amigo, movil_amigo, direccion_amigo, foto):
		
		#Identifiacion:
		self.carnet = carnet
		self.documento = documento
		self.tipo = tipo
		self.apellido = apellido
		self.nombre = nombre
		self.sexo = sexo
		self.carrera = carrera

		#Nacimiento:
		self.fecha = fecha
		self.pais = pais
		self.departamento = departamento
		self.ciudad = ciudad

		#Direccion y contacto:
		self.direccion = direccion
		self.barrio = barrio
		self.telefono_1 = telefono_1
		self.telefono_2 = telefono_2
		self.movil = movil
		self.email = email

		#Datos familiares
		self.apellido_familiar = apellido_familiar
		self.nombre_familiar = nombre_familiar
		self.telefono_familiar = telefono_familiar
		self.movil_familiar = movil_familiar
		self.direccion_familiar = direccion_familiar

		#Datos amigo
		self.apellido_amigo = apellido_amigo
		self.nombre_amigo = nombre_amigo
		self.telefono_amigo = telefono_amigo
		self.movil_amigo = movil_amigo
		self.direccion_amigo = direccion_amigo

		#Foto
		self.foto = foto


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:Lb1045723@localhost/Notas' 
app.config ['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)

class dbNotas(db.Model):
	__tablename__= 'tbl_notas'
	id = db.Column('id', db.Integer(), primary_key=True)
	carnet = db.Column('carnet', db.String())
	codigo = db.Column('codigo', db.String(), primary_key=True)
	materia = db.Column('materia', db.String())
	alumno = db.Column('alumno', db.String())
	periodo = db.Column('periodo', db.String())
	semestre = db.Column('semestre', db.String())
	np1 = db.Column('np1', db.Integer())
	np2 = db.Column('np2', db.Integer())
	np3 = db.Column('np3', db.Integer())
	np4 = db.Column('np4', db.Integer())
	np5 = db.Column('np5', db.Integer())